﻿using System;
using System.IO;

namespace Adventure
{
    public class Tools
    {

        #region Tools
        /// <summary>
        /// Пишет число в выделенной зоне
        /// </summary>
        /// <param name="num">Число которое будет писаться</param>
        /// <param name="length">Длина места для письма</param>
        /// <param name="color">Цвета которыми будут написана цифра Задний и Передний план соответственно</param>
        static public void NumPrint(int num, int length, ConsoleColor[] color)
        {
            string output = "";
            int lenghtOfNum = 1;
            int test = num;
            while (!(test < 10))
            {
                test /= 10;
                lenghtOfNum++;
            }
            for (int i = lenghtOfNum; i <= length; i++)
            {
                output += " ";
            }
            PO(output + num, color[0], color[1], false);
        }
        public static string[][] SplitForArray(string[] text)
        {
            string[][] WellDoneString = new string[text.Length][];
            //text[0].Split("\r\n")[0].ToCharArray().Length

            for (int i = 0; text.Length > i; i++)
            {
                WellDoneString[i] = text[i].Split("\r\n".ToCharArray());
            }
            return WellDoneString;
        }
        /// <summary>
        /// Выводит числвое значение в качестве полоски из символов
        /// </summary>
        /// <param name="numberStatesOfBar">количество симолов в полоске. Длинна этой полоски</param>
        /// <param name="currentValue">Текущее значение параметра</param>
        /// <param name="maxValue">Максимальное воозможное значение параметра</param>
        /// <param name="FirstSum">Основной символ для отрисовки полоски</param>
        /// <param name="SecondSum">Второстепенный символ ддля отрисовки. Используется когда значение не умещается в полную границу. Один символ = 10, текущее значение = 4</param>
        static public void BarPrint(int numberStatesOfBar, int currentValue, int maxValue, char FirstSum, char SecondSum)
        {
            if ((currentValue * (numberStatesOfBar / 2)) % maxValue == 0)
            {
                for (int i = 0; i < (currentValue * (numberStatesOfBar / 2)) / maxValue; i++)
                {
                    InfoBuffer.Output(FirstSum, 0);
                }
            }
            else
            {
                for (int i = 0; i < (currentValue * (numberStatesOfBar / 2)) / maxValue; i++)
                {
                    InfoBuffer.Output(FirstSum, 0);
                }
                InfoBuffer.Output(SecondSum, 0);
            }
        }
        static public void ChangeWindowSize(int width, int height)
        {
            Console.Clear();
            Console.SetWindowSize(1, 1);
            Console.SetBufferSize(width, height + 1);
            Console.SetWindowSize(width, height);
        }
        /// <summary>
        /// Красивый вывод с нужными цветами. при этом мы оставляем стандартные цвета не тронутыми
        /// </summary>
        /// <param name="text">Техе который выведется</param>
        /// <param name="col">Цвет вывода (формат 0-задний фон, 1-цвет строки)</param>
        /// <param name="writeLine">Нужно ли ставить символ "с новой строки" в конце вывода</param>
        static public void PO<T>(T text, ConsoleColor backgroundColor,ConsoleColor foregroundColor, bool writeLine)
        {
            ConsoleColor defb = Console.BackgroundColor;
            Console.BackgroundColor = backgroundColor;
            ConsoleColor deff = Console.ForegroundColor;
            Console.ForegroundColor = foregroundColor;
            InfoBuffer.Output(text, 0);
            Console.BackgroundColor = defb;
            Console.ForegroundColor = deff;
            if (writeLine)
                InfoBuffer.Output('\n', 0);
        }
        /// <summary>
        /// Считывает данные указанного файла
        /// </summary>
        /// <param name="path">Полный путь к файлу</param>
        /// <returns></returns>
        static public string ReadData(string path)
        {
            using (FileStream file = new FileStream(path, FileMode.Open))
            {
                using (StreamReader sr = new StreamReader(file))
                {
                    return sr.ReadToEnd();
                }
            }
        }
        /// <summary>
        /// Сохраняет файл в указанном месте
        /// </summary>
        /// <param name="path">Полный путь для сохранения, название и расширение файла</param>
        /// <param name="mode">По какому принципу записываются данные</param>
        /// <param name="text">Данные которые нужно сохранить</param>
        static public void SaveDate(string path, FileMode mode, string text)
        {
            using (FileStream file = new FileStream(path, FileMode.OpenOrCreate))
            {
                using (StreamWriter sw = new StreamWriter(file))
                {
                    sw.Write(text);
                }
            }
        }
        /// <summary>
        /// Возвращает значение нажатой кнопки
        /// </summary>
        /// <param name="waitKey">Нужно ли останавливать программу для ввода</param>
        /// <returns></returns>
        static public ConsoleKey WhatKey(bool waitKey)
        {
            if (!waitKey)
            {
                ConsoleKey key = ConsoleKey.Divide;
                if (Console.KeyAvailable)
                {
                    key = Console.ReadKey(true).Key;
                }
                return key;
            }
            else
            {
                ConsoleKey key = ConsoleKey.Divide;
                key = Console.ReadKey(true).Key;
                return key;
            }
        }
        /// <summary>
        /// Принимает символ и возвращает кнопку
        /// </summary>
        /// <param name="sum">Название символа в текстовом времени</param>
        /// <returns></returns>
        static public ConsoleKey WhatKey(string sum)
        {
            if (Enum.TryParse<ConsoleKey>(sum, out ConsoleKey gg))
                return gg;
            if (sum == "-")
                return ConsoleKey.OemMinus;
            if (sum == "\\")
                return ConsoleKey.Oem5;
            if (sum == "[")
                return ConsoleKey.Oem4;
            if (sum == "]")
                return ConsoleKey.Oem6;
            if (sum == ";")
                return ConsoleKey.Oem1;
            if (sum == "\'")
                return ConsoleKey.Oem7;
            if (sum == ",")
                return ConsoleKey.OemComma;
            if (sum == ".")
                return ConsoleKey.OemPeriod;
            if (sum == "/")
                return ConsoleKey.Oem2;
            if (sum == " ")
                return ConsoleKey.Spacebar;
            if (sum == "*")
                return ConsoleKey.OemPlus;
            return ConsoleKey.NoName;
        }
        #region Prints
        /// <summary>
        /// Вывод текста посимвольно с небольшой задержкой. между символами 50 мс. между строками 150 мс
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="a">Текст в любом формате</param>
        static public void Print<T>(T a)
        {
            char[] lol = a.ToString().ToCharArray();
            for (int i = 0; i < lol.Length; i++)
            {
                InfoBuffer.Output(lol[i], 0);
                System.Threading.Thread.Sleep(50);
            }
            System.Threading.Thread.Sleep(150);
            InfoBuffer.Output("\n", 0);
        }
        /// <summary>
        /// Вывод текста посимвольно с указанной задержкой
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="a">Текст в любом формате</param>
        /// <param name="WaitBetweenChar">Время задержки в мс между символами</param>
        /// <param name="WaitBetweenString">Время задержки в мс между строками</param>
        static public void Print<T>(T a, int WaitBetweenChar, int WaitBetweenString)
        {
            char[] lol = a.ToString().ToCharArray();
            for (int i = 0; i < lol.Length; i++)
            {
                InfoBuffer.Output(lol[i], 0);
                System.Threading.Thread.Sleep(WaitBetweenChar);
            }
            System.Threading.Thread.Sleep(WaitBetweenString);
            InfoBuffer.Output("\n", 0);
        }
        /// <summary>
        /// Выводит текст на определённых позициях в консоле(Координаты начинаются в левом верхнем углу и увеличиваются в сторону нижнего правого угла)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="a">Текст</param>
        /// <param name="widht">Координата [X]</param>
        /// <param name="height">Координата [Y]</param>
        static public void PrintOnPos<T>(T a, int widht, int height)
        {
            Console.SetCursorPosition(widht, height);
            InfoBuffer.Output(a, 0, false, new Vector2(widht, height));
        }
        static public void PrintCell(Cell cell)
        {
            PO(cell.CurrentSymbol,cell.CurrentBackgroundColor,cell.CurrentForegroundColor,false);
        }
        #endregion
        #endregion
    }
}
