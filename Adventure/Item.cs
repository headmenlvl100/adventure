﻿using System;

namespace Adventure
{
    public class Item
    {
        public char Symbol { get; set; }
        public bool UseOnTake { get; set; }
        public bool Disposable { get; set; }
        public byte[] Count { get; set; }
        public short[] AddMANA { get; set; }
        public short[] AddParameters { get; set; }
        public short[] AddSkills { get; set; }
        /*Fire Nature Water Earth Energy HP*/
        public byte Slot { get; set; }
        /*
         * 0 - none
         * 1 - cap
         * 2 - cloak
         * 3 - staff
         * 4 - necklace
         * 5 - right ring
         * 6 - left ring
         */
        public byte Chance { get; set; }
        public bool UnstandardColor { get; set; }
        public ConsoleColor BackgroundColor { get; set; }
        public ConsoleColor ForegroundColor { get; set; }
        public Item()
        {
            Symbol = '#';
            UseOnTake = false;
            Disposable = true;
            Count = new byte[] { 1, 99 };
            AddParameters = new short[] { 0, 0, 0, 0, 0, 0 };
            AddSkills = new short[] { 0, 0, 0, 0, 0, 0 };
            Slot = 0;
        }
        public Item(char symbol, bool useOnTake, bool disposable, byte[] count, short[] parameters, short[] skills, short[] mana, ConsoleColor backgroundColor,ConsoleColor foregroundColor, byte slot, byte chance)
        {
            #region Check
            if (parameters.Length != 6)
            {
                throw new Exception("Length of 'parameters' is invalid. It must be 6.");
            }
            if (count.Length != 2)
            {
                throw new Exception("Length of 'count' is invalid. It must be 2.");
            }
            if (skills.Length != 6)
            {
                throw new Exception("Length of 'skills' is invalid. It must be 6.");
            }
            if (mana.Length != 4)
            {
                throw new Exception("Length of 'mana' is invalid. It must be 4.");
            }
            #endregion
            Symbol = symbol;
            UseOnTake = useOnTake;
            Disposable = disposable;
            Count = count;
            AddParameters = parameters;
            AddSkills = skills;
            Slot = slot;
            Chance = chance;
            AddMANA = mana;
            BackgroundColor = backgroundColor;
            ForegroundColor = foregroundColor;
            UnstandardColor = true;
        }
    }
}
