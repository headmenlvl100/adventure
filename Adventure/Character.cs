﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure
{
    public static class Character
    {
        static public short[] parameters = new short[4] { 0, 0, 0, 0 };
        static public ushort[] MANA = new ushort[4] { 10, 10, 10, 10 };
        static public string nick = "VoidMaster";
        static public ushort[] hp = new ushort[2] { 20, 20 };
        static public ushort[] energy = new ushort[2] { 10, 10 };
        static public bool HPWasChanging = false;
        static public bool MANAWasChanging = false;
        static public bool EnergyWasChanging = false;
        static public ushort lvl = 0;
        static public Vector2 pos = new Vector2(0, 0);
        static public Vector2 lastPos = new Vector2(0,0);
        static public void ResetCharacter()
        {
            parameters = new short[4] { 0, 0, 0, 0 };
            MANA = new ushort[4] { 1000, 1000, 1000, 1000 };
            nick = "VoidMaster";
            hp = new ushort[2] { 20, 20 };
            energy = new ushort[2] { 10, 10 };
            HPWasChanging = false;
            MANAWasChanging = false;
            EnergyWasChanging = false;
            lvl = 0;
            pos = new Vector2(0, 0);
        }
    }
}
