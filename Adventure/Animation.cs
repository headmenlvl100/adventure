﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure
{
    class Animation
    {
        private string rawString;
        private string[] mediumString;
        private string[] wellString;
        public int animationSpeed;
        public int animationTime;
        readonly private string[][] animation;
        readonly public int[] animationSize = new int[2];
        private int animationStep = 0;
        public Vector2 position = new Vector2();
        private int gameSpeed;
        public void WriteAnimation(Vector2 pos, int gameTime)
        {
            if (animationTime >= animationSpeed)
            {
                for (int i = 0; i < animation[animationStep].Length; i++)
                {
                    InfoBuffer.Output(animation[animationStep][i], 0, false, new Vector2(pos.x, pos.y + i));
                }
                if (gameSpeed < animationSpeed)
                {
                    animationStep = (animationStep + (animationTime / animationSpeed)) % (animation.Length);
                }
                else
                {
                    animationStep = (animationStep + 1) % (animation.Length);
                }
                animationTime -= animationSpeed;
            }
            animationTime += gameTime;
            position = pos;
        }
        public void SetSpeed(int speed)
        {
            animationSpeed = speed;
        }
        public Animation(string name, int gamespeed)
        {
            try
            {
                rawString = Tools.ReadData(Directory.GetCurrentDirectory() + "\\Animations\\" + name + ".anim");
            }
            catch
            {
                Tools.PO($"Критическая ошибка: Файл {name}.anim не найден", Console.BackgroundColor, ConsoleColor.DarkRed, false);
                Environment.Exit(01);
                Console.ReadKey();
            }
            mediumString = rawString.Split(new string[] { "ʘ\r\n" }, StringSplitOptions.None);
            wellString = new string[mediumString.Length - 1];
            animationSpeed = int.Parse(mediumString[mediumString.Length - 1]);
            for (int i = 0; i < mediumString.Length - 1; i++)
            {
                wellString[i] = mediumString[i];
            }
            animationTime = animationSpeed;
            animation = Tools.SplitForArray(wellString);
            animationSize[0] = animation[0][0].ToCharArray().Length;
            animationSize[1] = animation[0].Length;
            gameSpeed = gamespeed;
        }
    }
}
