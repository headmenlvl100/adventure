﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure
{
    static public class InfoBuffer
    {
        public static string[] inputBuffer = new string[256];
        static public void Output<T>(T someData, byte channel, bool end, Vector2 pos)
        {
            switch (channel)
            {
                case 0:
                    if (!end)
                        for (int j = pos.x + pos.y * Console.WindowWidth; j < pos.x + pos.y * Console.WindowWidth + someData.ToString().Length; j++)
                        {
                            inputBuffer[channel].ToCharArray()[j] = someData.ToString().ToCharArray()[j];
                        }
                    else
                        Console.Write(inputBuffer[channel]);
                    break;
            }
        }
        static public void Output<T>(T someData, byte channel, bool end)
        {
            switch (channel)
            {
                case 0:
                    if (!end)
                        inputBuffer[channel] += someData;
                    else
                        Console.Write(inputBuffer[channel]);
                    break;
            }
        }

        static public void Output<T>(T someData, byte channel)
        {
            switch (channel)
            {
                case 0:
                        inputBuffer[channel] += someData;
                    break;
            }
        }
        static public void LoadingBuffer()
        {
            for(int i = 0; i<inputBuffer.Length; i++)
            {
                inputBuffer[i].PadRight(Console.BufferWidth*Console.BufferHeight);
            }
        }
    }
}
