﻿using System;
using System.Collections.Generic;
using static Adventure.Tools;

namespace Adventure
{
    public class Level
    {
        public Cell[,] map;
        /// <summary>
        /// Сырая карта из чисел
        /// </summary>
        public float[,] rawmap;
        private Perlin2D noise = new Perlin2D(WorldInfo.seed);
        public ConsoleColor defaultUIBackgroundColor = ConsoleColor.Black;
        public ConsoleColor defaultUIForegroundColor = ConsoleColor.DarkGray;
        public ConsoleColor defaultMapBackgroundColor = ConsoleColor.Black;
        public ConsoleColor defaultMapForegroundColor = ConsoleColor.DarkGray;
        public Cell outCell = new Cell();
        public LinkedList<Cell> BankOfCells = new LinkedList<Cell>();
        /// <summary>
        /// Генерирует новою карту
        /// </summary>
        /// <param name="x">Длина карты</param>
        /// <param name="y">Ширина карты</param>
        /// <param name="scale">Сила сжатия карты</param>
        public void Generate(int x, int y, int scale,Cell[] bank)
        {
            BankOfCells.Clear();
            foreach(var cell in bank)
            {
                AddCell(cell);
            }
            float[,] trawmap = new float[x * scale, y * scale];
            for (float i = 0; i < x * scale; i++)
            {
                for (float j = 0; j < y * scale; j++)
                {
                    trawmap[(int)i, (int)j] = (1 + noise.Noise((i + 1) / 10, (j + 1) / 10, 25, 0.2f)) / 2;
                }
            }
            float[,] fmap = new float[x, y];
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    for (int k = 0; k < scale; k++)
                    {
                        for (int l = 0; l < scale; l++)
                        {
                            fmap[i, j] += trawmap[i * scale + k, j * scale + l];
                        }
                    }
                    fmap[i, j] = (float)Math.Round(fmap[i, j] / (scale * scale) - 0.1f, 2);
                }
            }
            rawmap = fmap;
            map = new Cell[x, y];
            Cell theBest;
            LinkedList<Cell> currentPull = new LinkedList<Cell>();
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    foreach(var somecell in BankOfCells)
                    {
                        if (rawmap[i, j] >= somecell.MinSpawnRange && rawmap[i, j] <= somecell.MaxSpawnRange)
                            currentPull.AddLast(somecell);
                    }
                    if (currentPull.Count == 0)
                    {
                        map[i, j] = outCell;
                    }
                    else
                    {
                        theBest = currentPull.First.Value;
                        foreach (var somecell in currentPull)
                        {
                            if (Math.Abs((somecell.MaxSpawnRange + somecell.MinSpawnRange) / 2 - rawmap[i, j]) < Math.Abs((theBest.MaxSpawnRange + theBest.MinSpawnRange) / 2 - rawmap[i, j]))
                            {
                                theBest = somecell;
                            }
                        }
                        map[i, j] = theBest;
                    }
                    currentPull.Clear();
                }
            }
        }
        public bool AddCell(Cell somecell)
        {
            bool realyNewCell = true;
            foreach (var cell in BankOfCells)
            {
                if (somecell == cell)
                {
                    realyNewCell = false;
                    break;
                }
            }
            if (realyNewCell)
                BankOfCells.AddLast(somecell);
            return realyNewCell;
        }
        public Cell GiveCell(int x, int y)
        {
            if (x < 0 || y < 0)
                throw new Exception("Ты зачем вызываешь отрицательный элемент в массиве?");
            return map[x, y];
        }
    }
}
