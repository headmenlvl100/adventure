﻿namespace Adventure
{
    public class Vector2
    {
        public int x = 0;
        public int y = 0;
        public Vector2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public Vector2()
        {
            x = 0;
            y = 0;
        }

        public Vector2(Vector2 pos)
        {
            this.x = pos.x;
            this.y = pos.y;
        }
    }
}
