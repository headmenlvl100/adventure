﻿using System;
using System.IO;
using static Adventure.Tools;


namespace Adventure
{
    public static class Setting
    {
        static public ConsoleKey[] Keys = new ConsoleKey[6] { ConsoleKey.H, ConsoleKey.N, ConsoleKey.A, ConsoleKey.D, ConsoleKey.Spacebar, ConsoleKey.R };
        static public byte[] WindowsScale = new byte[2] { 30, 109 };
        /*

        0 - Heal
        1 - Next
        2 - Attack
        3 - Defence
        4 - End Move
        5 - Rest  

         */
        static public bool CheckSetting()
        {
            if (Keys[0] == Keys[1] || Keys[0] == Keys[2] || Keys[0] == Keys[3] || Keys[0] == Keys[4] || Keys[0] == Keys[5] || Keys[1] == Keys[2] || Keys[1] == Keys[3] || Keys[1] == Keys[4] || Keys[1] == Keys[5] || Keys[2] == Keys[3] || Keys[2] == Keys[4] || Keys[2] == Keys[5] || Keys[3] == Keys[4] || Keys[3] == Keys[5] || Keys[4] == Keys[5])
                return false;
            return true;
        }
        static public void SaveSetting()
        {
            string path = Directory.GetCurrentDirectory();
            File.Delete(Directory.GetCurrentDirectory() + @"\Setting.igv");
            SaveDate(path + @"\Setting.igv", FileMode.CreateNew, $"Hello\0{Keys[0]}\0{Keys[1]}\0{Keys[2]}\0{Keys[3]}\0{Keys[4]}\0{Keys[5]}");
        }
        static public void ReadSetting()
        {

            string path = Directory.GetCurrentDirectory();
            string[] data = ReadData(path + "\\Setting.igv").Split('\0');
            for (int i = 1; i < data.Length; i++)
            {
                Keys[i - 1] = WhatKey(data[i]);
            }
        }
    }
}
