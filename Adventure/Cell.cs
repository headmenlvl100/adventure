﻿using System;

namespace Adventure
{
    public class Cell
    {
        #region Current
        public char CurrentSymbol
        {
            get
            {
                if (HaveItem)
                    return ItemL.Symbol;
                else
                    return Symbol;
            }
        }
        public ConsoleColor CurrentBackgroundColor
        {
            get
            {
                if (HaveItem)
                {
                    return ItemL.BackgroundColor;
                }
                else
                {
                    return BackgroundColor;
                }
            }
        }
        public ConsoleColor CurrentForegroundColor
        {
            get
            {
                if (HaveItem)
                {
                    return ItemL.ForegroundColor;
                }
                else
                {
                    return ForegroundColor;
                }
            }
        }
        #endregion
        public char Symbol { get; set; }
        public ConsoleColor BackgroundColor { get; set; }
        public ConsoleColor ForegroundColor { get; set; }
        public bool UnstandardColor => BackgroundColor != Console.BackgroundColor || ForegroundColor != Console.ForegroundColor;
        public bool Obstacle { get; set; }
        public bool HaveItem => ItemL != null;  
        public Item ItemL { get; set; }
        public float MinSpawnRange { get; set; }
        public float MaxSpawnRange { get; set; }
        public byte Chance { get; set; }
        public Cell()
        {
            Symbol = '`';
            BackgroundColor = ConsoleColor.Black;
            ForegroundColor = ConsoleColor.Magenta;
            Obstacle = false;
            ItemL = null;
            MinSpawnRange = 0;
            MaxSpawnRange = 0;
            Chance = 255;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sym">Отображаемый символ(клетки)</param>
        /// <param name="obst">Является ли клетка препятствием</param>
        public Cell(char sym, ConsoleColor backgroundColor, ConsoleColor foregroundColor, bool obst, float minSpawnRange, float maxSpawnRange)
        {
            Symbol = sym;
            Obstacle = obst;
            ItemL = null;
            BackgroundColor = backgroundColor;
            ForegroundColor = foregroundColor;
            MinSpawnRange = minSpawnRange;
            MaxSpawnRange = maxSpawnRange;
            Chance = 255;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sym">Отображаемый символ(клетки)</param>
        /// <param name="obst">Является ли клетка препятствием</param>
        /// <param name="chance">Шанс спавна в процентах</param>
        public Cell(char sym, ConsoleColor backgroundColor, ConsoleColor foregroundColor, bool obst, float minSpawnRange, float maxSpawnRange, byte chance)
        {
            Symbol = sym;
            BackgroundColor = backgroundColor;
            ForegroundColor = foregroundColor;
            Obstacle = obst;
            ItemL = null;
            MinSpawnRange = minSpawnRange;
            MaxSpawnRange = maxSpawnRange;
            Chance = chance;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sym">Отображаемый символ(клетки)</param>
        /// <param name="obst">Является ли клетка препятствием</param>
        /// <param name="it">Предмет лежащий на клетке</param>
        public Cell(char sym, ConsoleColor backgroundColor, ConsoleColor foregroundColor, bool obst, float minSpawnRange, float maxSpawnRange, Item it, byte chance)
        {
            Symbol = sym;
            BackgroundColor = backgroundColor;
            ForegroundColor = foregroundColor;
            Obstacle = obst;
            ItemL = it;
            MinSpawnRange = minSpawnRange;
            MaxSpawnRange = maxSpawnRange;
            Chance = chance;
        }
        public Cell(Cell XD)
        {
            Symbol = XD.Symbol;
            BackgroundColor = XD.BackgroundColor;
            ForegroundColor = XD.ForegroundColor;
            Obstacle = XD.Obstacle;
            ItemL = XD.ItemL;
            MinSpawnRange = XD.MinSpawnRange;
            MaxSpawnRange = XD.MaxSpawnRange;
            Chance = XD.Chance;
        }
    }
}
