﻿using System;
using System.IO;
using System.Drawing;
using static Adventure.Tools;

namespace Adventure
{
    class Program
    {
        private static Perlin2D noise;
        char[,] LastUpdate = new char[Setting.WindowsScale[0], Setting.WindowsScale[1]];

        static void Main(string[] args)
        {
            InfoBuffer.LoadingBuffer();
            WorldInfo.rnd = new Random((int)((DateTime.Now.Ticks % uint.MaxValue) - (int.MaxValue)));
            Console.CursorVisible = true;
            try
            {
                Setting.ReadSetting();
            }
            catch (FileNotFoundException)
            {
                Setting.SaveSetting();
            }
            InfoBuffer.Output("Press any key to continue...\n", 0);
            ConsoleKey Key = ConsoleKey.Divide;
            while (Key == ConsoleKey.Divide)
                Key = WhatKey(true);
            

            Console.SetCursorPosition(0, 0);
            Console.CursorVisible = false;
            MainMenu();
        }
        static void MainMenu()
        {
            byte NOS = 0;
            Console.BackgroundColor = ConsoleColor.DarkGray;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            ChangeWindowSize(73, 30);/*
            Console.SetWindowSize(73, 30);
            Console.SetBufferSize(73, 31);*/
            InfoBuffer.Output("•", 0);
            Bye(73, 30);
            Console.SetCursorPosition(0, 0);
            InfoBuffer.Output("                                                            \n", 0);//0
            InfoBuffer.Output("                                                            \n", 0);//1
            InfoBuffer.Output("                    ╔═╗  ╔═╗  ╔════╗  ╔═╗ ╔════╗            \n", 0);//2
            InfoBuffer.Output("                    ╚╗╚╗╔╝╔╝ ╔╝╔══╗╚╗ ║ ║ ║╔══╗╚╗           \n", 0);//3
            InfoBuffer.Output("                     ╚╗╚╝╔╝  ║ ║  ║ ║ ║ ║ ║║  ║ ║           \n", 0);//4
            InfoBuffer.Output("                      ╚╗╔╝   ╚╗╚══╝╔╝ ║ ║ ║╚══╝╔╝           \n", 0);//5
            InfoBuffer.Output("                       ╚╝     ╚════╝  ╚═╝ ╚════╝            \n", 0);//6
            InfoBuffer.Output("                                                            \n", 0);//7
            InfoBuffer.Output("                                                            \n", 0);//8
            InfoBuffer.Output("           ╔═══╗  │  │  ┌───  │     ┌──┐                    \n", 0);//9
            InfoBuffer.Output("           ║ ¤ ║  ├──┤  ├───  │     ├──┘                    \n", 0);//10
            InfoBuffer.Output("           ╚═══╝  │  │  └───  └───  │                       \n", 0);//11
            InfoBuffer.Output("                                                            \n", 0);//12
            InfoBuffer.Output("           ╔═══╗  ┌─── ┌──┐ ├┐ │ ──┬── └──┘ ├┐ │ │  │ ┌───  \n", 0);//13
            InfoBuffer.Output("           ║   ║  │    │  │ │└┐│   │   ┌──┐ │└┐│ │  │ ├───  \n", 0);//14
            InfoBuffer.Output("           ╚═══╝  └─── └──┘ │ └┤   │   │  │ │ └┤ └──┘ └───  \n", 0);//15
            InfoBuffer.Output("                                                            \n", 0);//16
            InfoBuffer.Output("           ╔═══╗  ┌───  ──┬──  ┌──┐  ┌──┐  ──┬──            \n", 0);//17
            InfoBuffer.Output("           ║   ║  └──┐    │    │  │  ├─┬┘    │              \n", 0);//18
            InfoBuffer.Output("           ╚═══╝  ───┘    │    ├──┤  │ └┐    │              \n", 0);//19
            InfoBuffer.Output("                                                            \n", 0);//20
            InfoBuffer.Output("           ╔═══╗  ┌───  ┌───  ──┬── ──┬──  └──┘  ├┐ │  ┌─── \n", 0);//21
            InfoBuffer.Output("           ║   ║  └──┐  ├───    │     │    ┌──┐  │└┐│  │──┐ \n", 0);//22
            InfoBuffer.Output("           ╚═══╝  ───┘  └───    │     │    │  │  │ └┤  └──┘ \n", 0);//23
            InfoBuffer.Output("                                                            \n", 0);//24
            InfoBuffer.Output("           ╔═══╗  ┌───  └┐┌┘  └──┘  ──┬──                   \n", 0);//25
            InfoBuffer.Output("           ║   ║  ├───   ├┤   ┌──┐    │                     \n", 0);//26
            InfoBuffer.Output("           ╚═══╝  └───  ┌┘└┐  │  │    │                     \n", 0);//27
            InfoBuffer.Output("                                                            \n", 0);//28
            InfoBuffer.Output("(Arrow Up/Arrow Down) and (Spacebar)                        ", 0);//29
            {
              /*
                PrintOnPos("╔═╗  ╔═╗  ╔════╗  ╔═╗ ╔════╗", 20, Console.CursorTop + 5);
                PrintOnPos("╚╗╚╗╔╝╔╝ ╔╝╔══╗╚╗ ║ ║ ║╔══╗╚╗", 20, Console.CursorTop + 1);
                PrintOnPos(" ╚╗╚╝╔╝  ║ ║  ║ ║ ║ ║ ║║  ║ ║", 20, Console.CursorTop + 1);
                PrintOnPos("  ╚╗╔╝   ╚╗╚══╝╔╝ ║ ║ ║╚══╝╔╝", 20, Console.CursorTop + 1);
                PrintOnPos("   ╚╝     ╚════╝  ╚═╝ ╚════╝ ", 20, Console.CursorTop + 1);
                PrintOnPos("╔═══╗  │  │  ┌───  │     ┌──┐", 11, Console.CursorTop + 2);
                PrintOnPos("║ ¤ ║  ├──┤  ├───  │     ├──┘", 11, Console.CursorTop + 1);
                PrintOnPos("╚═══╝  │  │  └───  └───  │   ", 11, Console.CursorTop + 1);
                PrintOnPos("╔═══╗  ┌───  ──┬──  ┌──┐  ┌──┐  ──┬──", 11, Console.CursorTop + 2);
                PrintOnPos("║   ║  └──┐    │    │  │  ├─┬┘    │  ", 11, Console.CursorTop + 1);
                PrintOnPos("╚═══╝  ───┘    │    ├──┤  │ └┐    │  ", 11, Console.CursorTop + 1);
                PrintOnPos("╔═══╗  ┌───  ┌───  ──┬── ──┬──  └──┘  ├┐ │  ┌───", 11, Console.CursorTop + 2);
                PrintOnPos("║   ║  └──┐  ├───    │     │    ┌──┐  │└┐│  │──┐", 11, Console.CursorTop + 1);
                PrintOnPos("╚═══╝  ───┘  └───    │     │    │  │  │ └┤  └──┘", 11, Console.CursorTop + 1);
                PrintOnPos("╔═══╗  ┌───  └┐┌┘  └──┘  ──┬──", 11, Console.CursorTop + 2);
                PrintOnPos("║   ║  ├───   ├┤   ┌──┐    │  ", 11, Console.CursorTop + 1);
                PrintOnPos("╚═══╝  └───  ┌┘└┐  │  │    │  ", 11, Console.CursorTop + 1);
                Console.SetCursorPosition(0, 29);
                PO("(Arrow Up/Arrow Down) and (Spacebar)", new ConsoleColor[] { Console.BackgroundColor, ConsoleColor.Gray }, false);*/
            }
            while (true)
            {
                Bye(73, 30);
                Console.SetCursorPosition(0, 0);
                Vector2[] menu = new Vector2[5] { new Vector2(13, 10), new Vector2(13, 14), new Vector2(13, 18), new Vector2(13, 22), new Vector2(13, 26) };
                ConsoleKey key = WhatKey(true);
                if (key == ConsoleKey.UpArrow)
                {
                    Console.SetCursorPosition(menu[NOS].x, menu[NOS].y);
                    InfoBuffer.Output(" ", 0);
                    if (NOS != 0)
                        NOS--;
                    else
                        NOS = (byte)(menu.Length-1);
                    Console.SetCursorPosition(menu[NOS].x, menu[NOS].y);
                    InfoBuffer.Output("¤\n", 0);
                }
                if (key == ConsoleKey.DownArrow)
                {
                    Console.SetCursorPosition(menu[NOS].x, menu[NOS].y);
                    InfoBuffer.Output(" ", 0);
                    if (NOS != menu.Length - 1)
                        NOS++;
                    else
                        NOS = 0;
                    Console.SetCursorPosition(menu[NOS].x, menu[NOS].y);
                    InfoBuffer.Output("¤\n", 0);
                }
                if (key == ConsoleKey.Spacebar)
                {
                    switch (NOS)
                    {
                        case 0:
                            HelpInMainMenu();
                            break;
                        case 1:
                            Continue();
                            break;
                        case 2:
                            Start();
                            break;
                        case 3:
                            MenuSetting();
                            break;
                        case 4:
                            Exit();
                            break;
                    }
                }
                if (key == ConsoleKey.Escape)
                    Exit();
            }

        }
        static void HelpInMainMenu()
        {
            Console.Clear();
            Bye(73, 30);
            Console.SetBufferSize(73, 120);
            PrintOnPos("закрыто)", 13, 6);
            InfoBuffer.Output("\n", 0);
            Console.SetCursorPosition(9, 7);
            Print("Выход через:");
            Console.SetCursorPosition(21, 8);
            Print("5", 100, 0);
            Console.SetCursorPosition(21, 9);
            Print("4", 100, 0);
            Console.SetCursorPosition(21, 10);
            Print("3", 100, 0);
            Console.SetCursorPosition(21, 11);
            Print("2", 100, 0);
            Console.SetCursorPosition(21, 12);
            Print("1", 100, 0);
            Console.SetCursorPosition(21, 13);
            Print("0", 100, 0);
            MainMenu();
        }
        static void MenuSetting()
        {
            byte NOS = 0;
            Vector2[] menu = new Vector2[6] { new Vector2(8, 10), new Vector2(8, 12), new Vector2(8, 14), new Vector2(8, 16), new Vector2(8, 18), new Vector2(8, 20) };
            Console.BackgroundColor = ConsoleColor.DarkGray;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            ChangeWindowSize(73, 30);
            Console.SetCursorPosition(13, 4);
            PrintOnPos("┌───  ┌───  ──┬── ──┬──  └──┘  ├┐ │  ┌───", 13, Console.CursorTop);
            PrintOnPos("└──┐  ├───    │     │    ┌──┐  │└┐│  │──┐", 13, Console.CursorTop + 1);
            PrintOnPos("───┘  └───    │     │    │  │  │ └┤  └──┘", 13, Console.CursorTop + 1);
            PrintOnPos($"¤ Button for Heal ::::::::: '{Setting.Keys[0]}'", 8, Console.CursorTop + 4);
            PrintOnPos($"Button for Next ::::::::: '{Setting.Keys[1]}'", 10, Console.CursorTop + 2);
            PrintOnPos($"Button for Attack ::::::: '{Setting.Keys[2]}'", 10, Console.CursorTop + 2);
            PrintOnPos($"Button for Defence :::::: '{Setting.Keys[3]}'", 10, Console.CursorTop + 2);
            PrintOnPos($"Button for End Move ::::: '{Setting.Keys[4]}'", 10, Console.CursorTop + 2);
            PrintOnPos($"Button for Rest ::::::::: '{Setting.Keys[5]}'", 10, Console.CursorTop + 2);
            while (true)
            {
                Bye(73, 30);
                Console.SetCursorPosition(0, 0);
                ConsoleKey key = WhatKey(true);
                if (key == ConsoleKey.UpArrow)
                {
                    Console.SetCursorPosition(menu[NOS].x, menu[NOS].y);
                    InfoBuffer.Output(" ", 0);
                    if (NOS != 0)
                        NOS--;
                    else
                        NOS = 5;
                    Console.SetCursorPosition(menu[NOS].x, menu[NOS].y);
                    InfoBuffer.Output("¤", 0);
                }
                if (key == ConsoleKey.DownArrow)
                {
                    Console.SetCursorPosition(menu[NOS].x, menu[NOS].y);
                    InfoBuffer.Output(" ", 0);
                    if (NOS != 5)
                        NOS++;
                    else
                        NOS = 0;
                    Console.SetCursorPosition(menu[NOS].x, menu[NOS].y);
                    InfoBuffer.Output("¤", 0);
                }
                if (key == ConsoleKey.Spacebar)
                {
                    PrintOnPos("'press any key'", 36, 10 + NOS * 2);
                    Setting.Keys[NOS] = WhatKey(true);
                    while (!Setting.CheckSetting())
                    {
                        PrintOnPos("'press ANY key'", 36, 10 + NOS * 2);
                        Setting.Keys[NOS] = WhatKey(true);
                    }
                    PrintOnPos("                                  ", 36, 10 + NOS * 2);
                    PrintOnPos($"'{Setting.Keys[NOS]}'", 36, 10 + NOS * 2);
                    Setting.SaveSetting();
                }
                if (key == ConsoleKey.Escape)
                {
                    MainMenu();
                }

            }

        }
        static void Game(Level lvl, bool[,] mask)
        {
            char sus = 'ඞ';
            Console.Clear();
            Console.BackgroundColor = lvl.defaultUIBackgroundColor;
            Console.ForegroundColor = lvl.defaultUIForegroundColor;
            InfoBuffer.Output("╔═══════════════════════════════════════════════════════════════════════════════════════════════════════════╗", 0);//1
            InfoBuffer.Output("║ ╔═══════════════════════════════════════════════════════════════════════════════════════════════════════╗ ║", 0);//2
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//3
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//4
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//5
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//6
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//7
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//8
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//9
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//10
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//11
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//12
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//13
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//14
            InfoBuffer.Output("║ ║                                                   *                                                   ║ ║", 0);//15*
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//16
            InfoBuffer.Output("║ ║                                                                                                       ║ ║", 0);//17
            InfoBuffer.Output("║ ╠══════════════════╗                                                                                    ║ ║", 0);//18
            InfoBuffer.Output("║ ║ Health:          ║                                                                                    ║ ║", 0);//19
            InfoBuffer.Output("║ ║ Energy:          ║                                                                                    ║ ║", 0);//20
            InfoBuffer.Output("║ ║ ╔══════════════╗ ║                                                                                    ║ ║", 0);//21
            InfoBuffer.Output("║ ║ ║ M A N A      ║ ║                                                                                    ║ ║", 0);//22
            InfoBuffer.Output("║ ║ ║              ║ ║                                                                                    ║ ║", 0);//23
            InfoBuffer.Output("║ ║ ║              ║ ║                                                                                    ║ ║", 0);//24
            InfoBuffer.Output("║ ║ ║              ║ ║                                                                                    ║ ║", 0);//25
            InfoBuffer.Output("║ ║ ║              ║ ║                                                                                    ║ ║", 0);//26
            InfoBuffer.Output("║ ║ ╚══════════════╝ ║                                                                                    ║ ║", 0);//27
            InfoBuffer.Output("║ ╚══════════════════╩════════════════════════════════════════════════════════════════════════════════════╝ ║", 0);//28
            InfoBuffer.Output("╚═══════════════════════════════════════════════════════════════════════════════════════════════════════════╝", 0);//29
            InfoBuffer.Output("╫────┴────╫────┴────╫────┴────╫────┴────╫────┴────╫───*┴────╫────┴────╫────┴────╫────┴────╫────┴────╫────┴───", 0);//30
            Console.BackgroundColor = lvl.defaultUIBackgroundColor;
            Console.ForegroundColor = lvl.defaultUIForegroundColor;
            DrawMap(lvl, mask,true);                                                                                                                       //вывод мапы
            Console.SetCursorPosition(0, 0);

            Console.BackgroundColor = lvl.defaultUIBackgroundColor;
            Console.ForegroundColor = lvl.defaultUIForegroundColor;
            //MANA--------------------------------------------
            {
                Console.SetCursorPosition(6, 22);
                PO("Fire:", lvl.defaultUIBackgroundColor, ConsoleColor.DarkRed, false);
                Console.SetCursorPosition(6, 23);
                PO("Nature:",lvl.defaultUIBackgroundColor, ConsoleColor.DarkGreen, false);
                Console.SetCursorPosition(6, 24);
                PO("Water:",lvl.defaultUIBackgroundColor, ConsoleColor.DarkCyan, false);
                Console.SetCursorPosition(6, 25);
                PO("Earth:",lvl.defaultUIBackgroundColor, ConsoleColor.DarkYellow , false);
                //------------------------------------------------
                Console.SetCursorPosition(13, 22);
                NumPrint(Character.MANA[0], 4, new ConsoleColor[] { lvl.defaultUIBackgroundColor, ConsoleColor.DarkRed });
                Console.SetCursorPosition(13, 23);
                NumPrint(Character.MANA[1], 4, new ConsoleColor[] { lvl.defaultUIBackgroundColor, ConsoleColor.DarkGreen });
                Console.SetCursorPosition(13, 24);
                NumPrint(Character.MANA[2], 4, new ConsoleColor[] { lvl.defaultUIBackgroundColor, ConsoleColor.DarkCyan });
                Console.SetCursorPosition(13, 25);
                NumPrint(Character.MANA[3], 4, new ConsoleColor[] { lvl.defaultUIBackgroundColor, ConsoleColor.DarkYellow });
            }
            //HPBar-------------------------------------------
            {
                Console.SetCursorPosition(11, 18);
                BarPrint(18, Character.hp[0], Character.hp[1], '▒', '░');
            }
            //EnergyBar---------------------------------------
            {
                Console.SetCursorPosition(11, 19);
                BarPrint(18, Character.energy[0], Character.energy[1], '▒', '░');
            }
            Console.SetCursorPosition(0, 0);
            while (true)
            {
                Bye(Setting.WindowsScale[1], Setting.WindowsScale[0]);
                ConsoleKey key = WhatKey(false);
                if (Character.MANAWasChanging)
                {
                    Console.SetCursorPosition(13, 22);
                    NumPrint(Character.MANA[0], 4, new ConsoleColor[] { lvl.defaultUIBackgroundColor, ConsoleColor.DarkRed });
                    Console.SetCursorPosition(13, 23);
                    NumPrint(Character.MANA[1], 4, new ConsoleColor[] { lvl.defaultUIBackgroundColor, ConsoleColor.DarkGreen });
                    Console.SetCursorPosition(13, 24);
                    NumPrint(Character.MANA[2], 4, new ConsoleColor[] { lvl.defaultUIBackgroundColor, ConsoleColor.DarkCyan });
                    Console.SetCursorPosition(13, 25);
                    NumPrint(Character.MANA[3], 4, new ConsoleColor[] { lvl.defaultUIBackgroundColor, ConsoleColor.DarkYellow });
                }
                if (Character.HPWasChanging)
                {
                    Character.HPWasChanging = false;
                    Console.SetCursorPosition(11, 18);
                    BarPrint(18, Character.hp[1], Character.hp[1], ' ', ' ');
                    Console.SetCursorPosition(11, 18);
                    BarPrint(18, Character.hp[0], Character.hp[1], '▒', '░');
                }
                if (Character.EnergyWasChanging)
                {
                    Character.EnergyWasChanging = false;
                    Console.SetCursorPosition(11, 19);
                    BarPrint(18, Character.energy[1], Character.energy[1], ' ', ' ');
                    Console.SetCursorPosition(11, 19);
                    BarPrint(18, Character.energy[0], Character.energy[1], '▒', '░');
                }
                if (key == ConsoleKey.Escape)
                {
                    MainMenu();
                }
                Console.BackgroundColor = ConsoleColor.DarkGray;
                Console.ForegroundColor = ConsoleColor.Black;
                //Move----------------------------------------
                {
                    switch (key)
                    {
                        case ConsoleKey.UpArrow:
                            {
                                Move(lvl, 1);
                                DrawMap(lvl, mask,false);
                                break;
                            }
                        case ConsoleKey.RightArrow:
                            {
                                Move(lvl, 2);
                                DrawMap(lvl, mask,false);
                                break;
                            }
                        case ConsoleKey.DownArrow:
                            {
                                Move(lvl, 3);
                                DrawMap(lvl, mask,false);
                                break;
                            }
                        case ConsoleKey.LeftArrow:
                            {
                                Move(lvl, 4);
                                DrawMap(lvl, mask,false);
                                break;
                            }
                    }
                }
            }
        }
        static void Start()
        {
            bool generation = true;
            Animation loading = new Animation("Loading",10);
            System.Threading.Tasks.Task animationPlayer = new System.Threading.Tasks.Task(() => 
            {
                while(generation)
                {
                    loading.WriteAnimation(new Vector2(30, 12), 100);
                    System.Threading.Thread.Sleep(100);
                }
            });
            Console.Clear();
            animationPlayer.Start();
            if (WorldInfo.seed == new int())
                WorldInfo.seed = WorldInfo.rnd.Next(int.MinValue, int.MaxValue);
            noise = new Perlin2D(WorldInfo.seed);
            Level lvl1 = new Level();
            Cell[] bankOfCells = new Cell[]
            {
                new Cell('█', lvl1.defaultMapBackgroundColor, lvl1.defaultMapForegroundColor, false, 0.40f, 1.00f),

                new Cell('▓', lvl1.defaultMapBackgroundColor, lvl1.defaultMapForegroundColor, false, 0.36f, 0.40f),

                new Cell('▒', lvl1.defaultMapBackgroundColor, lvl1.defaultMapForegroundColor, false, 0.34f, 0.36f),

                new Cell('░', lvl1.defaultMapBackgroundColor, lvl1.defaultMapForegroundColor, false, 0.32f, 0.34f),

                new Cell(' ', lvl1.defaultMapBackgroundColor, lvl1.defaultMapForegroundColor, true, 0.00f, 0.32f),

                new Cell('G', ConsoleColor.DarkCyan, ConsoleColor.Green, true, 0.40f,0.40f)
            };
            lvl1.Generate(1090, 300, 1,bankOfCells);
            ChangeWindowSize(Setting.WindowsScale[1], Setting.WindowsScale[0]);
            bool[,] mask = new bool[Setting.WindowsScale[0], Setting.WindowsScale[1]];
            #region REGmask
            string[] maskButNotMask = { "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                                        "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0000000000000000000000111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0000000000000000000000111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0000000000000000000000111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0000000000000000000000111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0000000000000000000000111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0000000000000000000000111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0000000000000000000000111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0000000000000000000000111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0000000000000000000000111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0000000000000000000000111111111111111111111111111111111111111111111111111111111111111111111111111111111111000",
                                        "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                                        "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                                        "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                                                                                                                                                            };
            Console.Clear();
            for (int i = 0; i < maskButNotMask.Length; i++)
            {
                for (int j = 0; j < maskButNotMask[0].Length; j++)
                {
                    if (maskButNotMask[i][j] == '0')
                    {
                        mask[i, j] = false;
                    }
                    else
                    {
                        mask[i, j] = true;
                    }
                }
            }
            #endregion
            generation = false;
            System.Threading.Thread.Sleep(150);
            Game(lvl1, mask);
        }
        static void Continue()
        {
            Console.Clear();
            Bye(73, 30);
            Console.SetBufferSize(73, 120);
            PrintOnPos("закрыто)", 13, 6);
            InfoBuffer.Output("\n", 0);
            Console.SetCursorPosition(9, 7);
            Print("Выход через:");
            Console.SetCursorPosition(21, 8);
            Print("5", 100, 0);
            Console.SetCursorPosition(21, 9);
            Print("4", 100, 0);
            Console.SetCursorPosition(21, 10);
            Print("3", 100, 0);
            Console.SetCursorPosition(21, 11);
            Print("2", 100, 0);
            Console.SetCursorPosition(21, 12);
            Print("1", 100, 0);
            Console.SetCursorPosition(21, 13);
            Print("0", 100, 0);
            MainMenu();
        }
        static void DrawMap(Level lvl, bool[,] mask,bool FirstDraw)
        {
            Console.BackgroundColor = lvl.defaultMapBackgroundColor;
            Console.ForegroundColor = lvl.defaultMapForegroundColor;

            int IndentX = (int)((Setting.WindowsScale[1] / 2) + 0.5f);
            int IndentY = (int)((Setting.WindowsScale[0] / 2) + 0.5f)-1;

            //(j - IndentX + Character.pos.x)
            //(i - IndentY + Character.pos.y)

            int posX = 0;
            int posY = 0;

            int prePosX = 0;
            int prePosY = 0;


            Console.SetCursorPosition(0, 0);

            for (int i = 0; i < Setting.WindowsScale[0]; i++) 
            {
                Console.SetCursorPosition(0, i);
                for (int j = 0; j < Setting.WindowsScale[1]; j++) 
                {
                    Console.CursorVisible = true;
                    posX = j - IndentX + Character.pos.x;
                    posY = i - IndentY + Character.pos.y;
                    prePosX = j - IndentX + Character.lastPos.x;
                    prePosY = i - IndentY + Character.lastPos.y;
                    if (mask[i, j] == true)
                    {
                        if (i == IndentY && j == IndentX) 
                        {
                            PrintCell(new Cell('☻',lvl.defaultMapBackgroundColor,lvl.defaultMapForegroundColor,false,0,100));
                        }
                        else
                        {
                            if (!(posX < 0 || posY < 0 || posX >= lvl.map.GetLength(1) || posY >= lvl.map.GetLength(0)))
                            {
                                if (!(prePosX < 0 || prePosY < 0 || prePosX >= lvl.map.GetLength(1) || prePosY >= lvl.map.GetLength(0)))
                                {
                                    if (!(lvl.GiveCell(prePosX, prePosY) == lvl.GiveCell(posX, posY))||FirstDraw)
                                    {
                                        PrintCell(lvl.GiveCell(posX, posY));
                                    }
                                    else
                                    {
                                        Console.SetCursorPosition(Console.CursorLeft + 1, Console.CursorTop);
                                    }
                                }
                                else
                                {
                                    PrintCell(lvl.GiveCell(posX, posY));
                                }
                            }
                            else
                            {
                                if (!(prePosX < 0 || prePosY < 0 || prePosX >= lvl.map.GetLength(1) || prePosY >= lvl.map.GetLength(0))||FirstDraw)
                                    PrintCell(lvl.outCell);
                                else
                                    Console.SetCursorPosition(Console.CursorLeft + 1, Console.CursorTop);
                            }
                        }
                    }
                    else
                    {

                        if(Console.CursorLeft+2<=Console.WindowWidth)
                            Console.SetCursorPosition(Console.CursorLeft+1, Console.CursorTop);
                    }
                }
            }
            Console.BackgroundColor = lvl.defaultUIBackgroundColor;
            Console.ForegroundColor = lvl.defaultUIForegroundColor;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lvl"></param>
        /// <param name="direction">min=0 max=4 1-up 4-left 0-stay</param>
        static void Move(Level lvl, byte direction)
        {
            switch (direction)
            {
                case 1:
                    Character.lastPos = new Vector2(Character.pos);
                    if (Character.pos.y == 0 || (lvl.map[Character.pos.x, Character.pos.y - 1].Obstacle && !lvl.map[Character.pos.x, Character.pos.y].Obstacle))
                        break;
                    Character.pos.y -= 1;
                    break;
                case 2:
                    Character.lastPos = new Vector2(Character.pos);
                    if (Character.pos.x == lvl.map.GetLength(0) - 1 || (lvl.map[Character.pos.x + 1, Character.pos.y].Obstacle && !lvl.map[Character.pos.x, Character.pos.y].Obstacle))
                        break;
                    Character.pos.x += 1;
                    break;
                case 3:
                    Character.lastPos = new Vector2(Character.pos);
                    if (Character.pos.y == lvl.map.GetLength(1) - 1 || (lvl.map[Character.pos.x, Character.pos.y + 1].Obstacle && !lvl.map[Character.pos.x, Character.pos.y].Obstacle))
                        break;
                    Character.pos.y += 1;
                    break;
                case 4:
                    Character.lastPos = new Vector2(Character.pos);
                    if (Character.pos.x == 0 || (lvl.map[Character.pos.x - 1, Character.pos.y].Obstacle && !lvl.map[Character.pos.x, Character.pos.y].Obstacle))
                        break;
                    Character.pos.x -= 1;
                    break;
                case 0:
                    //after adding enemy
                    break;
            }
            Cell currentCell = lvl.map[Character.pos.x, Character.pos.y];
            if (currentCell.HaveItem)
            {
                if (currentCell.ItemL.UseOnTake == true)
                {
                    for (int i = 0; i < 4; i++)
                        if ((Character.MANA[i] >= Math.Abs(currentCell.ItemL.AddMANA[i])) || currentCell.ItemL.AddMANA[i]>0)
                            Character.MANA[i] = (ushort)(Character.MANA[i] + currentCell.ItemL.AddMANA[i]);
                        else
                        {
                            int buffer = Math.Abs(currentCell.ItemL.AddMANA[i]) - Character.MANA[i];
                            Character.MANA[i] = 0;
                            if (Character.hp[0] - (ushort)(buffer / 2) < 0)
                            {
                                WorldInfo.rnd = new Random((int)((DateTime.Now.Ticks % uint.MaxValue) - (int.MaxValue)));
                                Character.ResetCharacter();
                                MainMenu();
                            }
                            Character.hp[0] -= (ushort)(buffer / 2);
                            Character.HPWasChanging = true;
                        }
                    Character.MANAWasChanging = true;
                    currentCell.ItemL = new Item();
                    if (Character.MANA[0] > 100 && Character.MANA[1] > 100 && Character.MANA[2] > 100 && Character.MANA[3] > 100)
                        Win();
                }
            }
        }
        static void Bye(int x, int y)
        {
            if (Console.WindowHeight != y || Console.WindowWidth != x)
            if (Console.WindowHeight != y || Console.WindowWidth != x)
            {
                Console.Clear();
                Environment.Exit(228);
            }
        }
        static void Win()
        {
            Console.Clear();
            InfoBuffer.Output("•\n", 0);
            InfoBuffer.Output("├┐ │ └──┘ ┌─── ┌─── \n", 0);
            InfoBuffer.Output("│└┐│ ┌──┐ │    ├─── \n", 0);
            InfoBuffer.Output("│ └┤ │  │ └─── └─── \n", 0);
            InfoBuffer.Output("                    \n", 0);
            InfoBuffer.Output("┌─── ┌──┐ ┌─┬─┐ ┌───\n", 0);
            InfoBuffer.Output("│──┐ │  │ │ │ │ ├───\n", 0);
            InfoBuffer.Output("└──┘ ├──┤ │ │ │ └───\n", 0);
            InfoBuffer.Output("   ╔════╗   ╔════╗  \n", 0);
            InfoBuffer.Output("  ╔╝    ╚╗ ╔╝    ╚╗ \n", 0);
            InfoBuffer.Output(" ╔╝      ╚═╝      ╚╗\n", 0);
            InfoBuffer.Output(" ║                 ║\n", 0);
            InfoBuffer.Output(" ╚╗               ╔╝\n", 0);
            InfoBuffer.Output("  ╚═╗           ╔═╝ \n", 0);
            InfoBuffer.Output("    ╚══╗     ╔══╝   \n", 0);
            InfoBuffer.Output("       ╚═╗ ╔═╝      \n", 0);
            InfoBuffer.Output("         ╚═╝        \n", 0);
            InfoBuffer.Output("Press any key...    \n", 0);
            Console.ReadKey();
            Environment.Exit(1337);
        }
        static void Exit()
        {
            Console.Clear();
            InfoBuffer.Output("┌──┐  │  │ ┌───     \n", 0);
            InfoBuffer.Output("├──┴┐ └──┤ ├───     \n", 0);
            InfoBuffer.Output("└───┘ └──┘ └───     \n", 0);
            InfoBuffer.Output("   ╔════╗   ╔════╗  \n", 0);
            InfoBuffer.Output("  ╔╝    ╚╗ ╔╝    ╚╗ \n", 0);
            InfoBuffer.Output(" ╔╝      ╚═╝      ╚╗\n", 0);
            InfoBuffer.Output(" ║                 ║\n", 0);
            InfoBuffer.Output(" ╚╗               ╔╝\n", 0);
            InfoBuffer.Output("  ╚═╗           ╔═╝ \n", 0);
            InfoBuffer.Output("    ╚══╗     ╔══╝   \n", 0);
            InfoBuffer.Output("       ╚═╗ ╔═╝      \n", 0);
            InfoBuffer.Output("         ╚═╝        \n", 0);
            InfoBuffer.Output("Press any key...    \n", 0);
            Console.ReadKey();
            Environment.Exit(1337);
        }
        //█ ▌
    }

    /*
InfoBuffer.Output("                                                            ");
InfoBuffer.Output("                                                            ");
InfoBuffer.Output("                    ╔═╗  ╔═╗  ╔════╗  ╔═╗ ╔════╗            ");
InfoBuffer.Output("                    ╚╗╚╗╔╝╔╝ ╔╝╔══╗╚╗ ║ ║ ║╔══╗╚╗           ");
InfoBuffer.Output("                     ╚╗╚╝╔╝  ║ ║  ║ ║ ║ ║ ║║  ║ ║           ");
InfoBuffer.Output("                      ╚╗╔╝   ╚╗╚══╝╔╝ ║ ║ ║╚══╝╔╝           ");
InfoBuffer.Output("                       ╚╝     ╚════╝  ╚═╝ ╚════╝            ");
InfoBuffer.Output("                                                            ");
InfoBuffer.Output("           ╔═══╗  │  │  ┌───  │     ┌──┐                    ");
InfoBuffer.Output("           ║ ¤ ║  ├──┤  ├───  │     ├──┘                    ");
InfoBuffer.Output("           ╚═══╝  │  │  └───  └───  │                       ");
InfoBuffer.Output("                                                            ");
InfoBuffer.Output("           ╔═══╗  ┌─── ┌──┐ ├┐ │ ──┬── └──┘ ├┐ │ │  │ ┌───  ");          
InfoBuffer.Output("           ║   ║  │    │  │ │└┐│   │   ┌──┐ │└┐│ │  │ ├───  ");     
InfoBuffer.Output("           ╚═══╝  └─── └──┘ │ └┤   │   │  │ │ └┤ └──┘ └───  ");         
InfoBuffer.Output("                                                            ");
InfoBuffer.Output("           ╔═══╗  ┌───  ──┬──  ┌──┐  ┌──┐  ──┬──            ");
InfoBuffer.Output("           ║   ║  └──┐    │    │  │  ├─┬┘    │              ");
InfoBuffer.Output("           ╚═══╝  ───┘    │    ├──┤  │ └┐    │              ");
InfoBuffer.Output("                                                            ");
InfoBuffer.Output("           ╔═══╗  ┌───  ┌───  ──┬── ──┬──  └──┘  ├┐ │  ┌─── ");
InfoBuffer.Output("           ║   ║  └──┐  ├───    │     │    ┌──┐  │└┐│  │──┐ ");
InfoBuffer.Output("           ╚═══╝  ───┘  └───    │     │    │  │  │ └┤  └──┘ ");
InfoBuffer.Output("                                                            ");
InfoBuffer.Output("           ╔═══╗  ┌───  └┐┌┘  └──┘  ──┬──                   ");
InfoBuffer.Output("           ║   ║  ├───   ├┤   ┌──┐    │                     ");
InfoBuffer.Output("           ╚═══╝  └───  ┌┘└┐  │  │    │                     ");
InfoBuffer.Output("                                                            ");
InfoBuffer.Output("                                                            ");
InfoBuffer.Output("(Arrow Up/Arrow Down) and (Spacebar)                        ");
---------------------------------------------------





                    ╔═╗  ╔═╗  ╔════╗  ╔═╗ ╔════╗
                    ╚╗╚╗╔╝╔╝ ╔╝╔══╗╚╗ ║ ║ ║╔══╗╚╗
                     ╚╗╚╝╔╝  ║ ║  ║ ║ ║ ║ ║║  ║ ║
                      ╚╗╔╝   ╚╗╚══╝╔╝ ║ ║ ║╚══╝╔╝
                       ╚╝     ╚════╝  ╚═╝ ╚════╝

           ╔═══╗  │  │  ┌───  │     ┌──┐
           ║ ¤ ║  ├──┤  ├───  │     ├──┘
           ╚═══╝  │  │  └───  └───  │

           ╔═══╗  ┌───  ──┬──  ┌──┐  ┌──┐  ──┬──
           ║   ║  └──┐    │    │  │  ├─┬┘    │
           ╚═══╝  ───┘    │    ├──┤  │ └┐    │

           ╔═══╗  ┌───  ┌───  ──┬── ──┬──  └──┘  ├┐ │  ┌───
           ║   ║  └──┐  ├───    │     │    ┌──┐  │└┐│  │──┐
           ╚═══╝  ───┘  └───    │     │    │  │  │ └┤  └──┘

           ╔═══╗  ┌───  └┐┌┘  └──┘  ──┬──
           ║   ║  ├───   ├┤   ┌──┐    │
           ╚═══╝  └───  ┌┘└┐  │  │    │



(Arrow Up/Arrow Down) and (Spacebar)
--------
    continue

         ╗ ╝ ╚ ╔ ╣ ║ ╠ ╬ ╩ ═ ╦ 

  ┤ │ ┐ └ ┴ ┬ ├ ─ ╫ ┼ ╫ ╪ ┘ ┌

    ┌──┐  │  │ ┌───
    ├──┴┐ └──┤ ├───
    └───┘ └──┘ └───
                                       

                  1   ╔════╗   ╔════╗                        
                  2  ╔╝    ╚╗ ╔╝    ╚╗ 
                  3 ╔╝      ╚═╝      ╚╗
                  4 ║                 ║
                  5 ╚╗               ╔╝
                  6  ╚═╗           ╔═╝
                  7    ╚══╗     ╔══╝
                  8       ╚═╗ ╔═╝
                  9         ╚═╝
                   

    ______________


    good game

    InfoBuffer.Output"├┐ │ └──┘ ┌─── ┌─── ");
    InfoBuffer.Output"│└┐│ ┌──┐ │    ├─── ");
    InfoBuffer.Output"│ └┤ │  │ └─── └─── ");
    InfoBuffer.Output"                    ");
    InfoBuffer.Output"┌─── ┌──┐ ┌─┬─┐ ┌───");
    InfoBuffer.Output"│──┐ │  │ │ │ │ ├───");
    InfoBuffer.Output"└──┘ ├──┤ │ │ │ └───");
    ______________
    



             ┌───  ┌───  ──┬── ──┬──  └──┘  ├┐ │  ┌───
             └──┐  ├───    │     │    ┌──┐  │└┐│  │──┐
             ───┘  └───    │     │    │  │  │ └┤  └──┘



        ¤ Button for Heal ::::::::: 'H'

          Button for Next ::::::::: 'N'

          Button for Attack ::::::: 'A'

          Button for Defence :::::: 'D'

          Button for End Move ::::: 'Spacebar'

          Button for Rest ::::::::: 'R'









    ______________
                                                                                                                        TODO:   1)Пофиксить переменные маны и сделать зависимость от хп
                                                                                                                                2)Прописать Скорость и Энергию
                                                                                                                                3)Сделать простого противника
                                                                                                                                4)Сделать первые скилы
    */
}
